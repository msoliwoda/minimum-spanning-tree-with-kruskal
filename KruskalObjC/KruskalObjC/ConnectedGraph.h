//
//  ConnectedGraph.h
//  KruskalObjC
//
//  Created by Mateusz Soliwoda on 15.05.2015.
//  Copyright (c) 2015 Mateusz Soliwoda. All rights reserved.
//
//  s13215@pja.edu.pl

#import <Foundation/Foundation.h>
#import "Edge.h"

@interface ConnectedGraph : NSObject

@property (nonatomic, strong) NSMutableArray *edges;
@property (nonatomic, strong) NSMutableSet *vertexes;

- (void)sortEdgesIncreasing;

- (NSString *)edgesString;

@end
