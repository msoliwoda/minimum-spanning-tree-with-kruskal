//
//  KruskalAlgorithm.m
//  KruskalObjC
//
//  Created by Mateusz Soliwoda on 15.05.2015.
//  Copyright (c) 2015 Mateusz Soliwoda. All rights reserved.
//
//  s13215@pja.edu.pl
//
//  Przykładowy graf w pliku Json pochodzi ze strony: http://edu.i-lo.tarnow.pl/inf/alg/001_search/0141.php
//
//  Wagi grafu powinny być dodatnie.
//
//


#import "KruskalAlgorithm.h"
#import "ConnectedGraph.h"
#import "MinimumSpanningTree.h"

static NSString *kJsonGraphFileName = @"graph";

@interface KruskalAlgorithm()

@property (nonatomic, strong) ConnectedGraph *graph;
@property (nonatomic, strong) MinimumSpanningTree *minimumSpanningTree;
@property (nonatomic, strong) NSMutableSet *vertexGroups;
//@property (nonatomic, strong) NSMutableArray *minimumSpanningTree;

@end

@implementation KruskalAlgorithm

- (instancetype)init
{
    self = [super init];
    if (self) {
        // Inicjalizacja zbioru niepowtarzalnych grup wierzchołków
        _vertexGroups = [[NSMutableSet alloc] init];
        
        // Inicjalizacja minimalnego drzewa rozpinającego
        _minimumSpanningTree = [[MinimumSpanningTree alloc] init];
        
    }
    return self;
}


/**
 *  Getter obiektu 'graph'.
 *
 *  @return - obiekt type ConnectedGraph na podstawie danych z pliku *.json
 */
- (ConnectedGraph *)graph {
    
    if (_graph == nil) {
        _graph = [[ConnectedGraph alloc] init];
        
        NSString *jsonPath = [[NSBundle mainBundle] pathForResource:kJsonGraphFileName ofType:@"json"];
        NSURL *jsonURL = [NSURL fileURLWithPath:jsonPath];
        NSError *jsonReadingError = nil;
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfURL:jsonURL] options:kNilOptions error:&jsonReadingError];
        
        if (jsonData != nil) {
            
            // Edges
            NSArray *edgesFromJson = [jsonData objectForKey:@"_edges"];
            NSMutableArray *edges = [[NSMutableArray alloc] init];
            NSMutableSet *vertexes = [[NSMutableSet alloc] init];
            for (int i = 0; i < edgesFromJson.count; i++) {
                int sourceVert = [[edgesFromJson[i] objectForKey:@"_a"] intValue];
                int destVert   = [[edgesFromJson[i] objectForKey:@"_b"] intValue];
                int weight     = [[edgesFromJson[i] objectForKey:@"_w"] intValue];
                
                Edge *e = [[Edge alloc] initWithSourceVert:sourceVert destVert:destVert weight:weight];
                [edges addObject:e];
                
                [vertexes addObjectsFromArray:@[@(sourceVert), @(destVert)]];
            }
            _graph.edges = edges;
            _graph.vertexes = vertexes;
            
//            // Vertexes
//            NSArray *vertexesFromJson = [jsonData objectForKey:@"_vertexes"];
//            NSMutableSet *vertexes = [[NSMutableSet alloc] init];
//            for (int i = 0; i < vertexesFromJson.count; i++) {
//                int sourceVert = [[edgesFromJson[i] objectForKey:@"_a"] intValue];
//                int destVert   = [[edgesFromJson[i] objectForKey:@"_b"] intValue];
//                
//                [_graph.vertexes addObject:@(sourceVert)];
//            }
//            _graph.edges = edges;
        }
    }
    
    return _graph;
}

/**
 *  Metoda sprawdzająca, czy istnieje zbiór, który zawiera dany wierzchołek.
 *
 *  @param - vertex numer wierzchołka
 *
 *  @return - wskaźnik na grupę zwierającą wierzchołek
 */
- (id)groupThatContainsVertex:(int)vertex
{
    for (NSMutableSet *group in self.vertexGroups) {
        if ([group containsObject:@(vertex)]) {
            return group;
        }
    }
    
    return nil;
}

/**
 *  Metoda wyliczająca końcowy koszt drzewa.
 *
 *  @return - końcowy koszt - suma wag
 */
- (int)totalCostOfMinimumSpanningTree
{
    int cost = ({
        int i = 0;
        for (Edge *e in self.minimumSpanningTree.edges) {
            i += e.weight;
        }
        i;
    });
    
    return cost;
}

/**
 *  Wypisanie wyniku w konsoli
 */

- (void)printResult
{
    NSMutableString *resultString = [[NSMutableString alloc] initWithString:@"\n\n----- (Source vertex, Destination vertex) : Edge Weight\n"];
    for (Edge *e in [self.minimumSpanningTree.edges sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"weight" ascending:YES]]]) {
        [resultString appendString:[e toString]];
    }
    
    [resultString appendString:[NSString stringWithFormat:@"----- Total cost of Minimum Spanning Tree: %ld", (unsigned long)[self totalCostOfMinimumSpanningTree]]];
    
    NSLog(@"%@", resultString);
}

/**
 *  Metoda wykonująca algorytm Kruskala na danych pochodzących z pliku *.json
 *  W celu uniknięcia cykli stworzyłem zbiory rozłączne.
 */
- (void)start
{
    NSLog(@"\n\n----- ALL INPUT GRAPH EDGES:\n%@\n\n", [self.graph edgesString]);
    
    [self.graph sortEdgesIncreasing];
    
    for (Edge *edge in self.graph.edges) {
        
        int sourceVertex = edge.sourceVert;
        int destVertex = edge.destVert;
        
        NSMutableSet *sourceVertexGroup = [self groupThatContainsVertex:sourceVertex];
        NSMutableSet *destVertexGroup = [self groupThatContainsVertex:destVertex];
        
        //NSLog(@"%@", [edge toString]);
        //NSLog(@"GROUPS: %@", self.vertexGroups);
        
        if (!sourceVertexGroup) {
            
            // Wygląda na to, że pierwszy wierzchołek nie zawiera się w żadnej grupie, więc krawędź mogę dodać do drzewa.
            [self.minimumSpanningTree addEdge:edge];
            
            // Jeśli drugi wierzchołek również nie zawiera się w żadnej grupie, to obydwa wierzchołki dodaję do nowej grupy
            // a samą grupę dodaję do zbioru wszystkich grup
            if (!destVertexGroup) {
                NSMutableSet *newGroup = [[NSMutableSet alloc] initWithObjects:@(sourceVertex), @(destVertex), nil];
                [self.vertexGroups addObject:newGroup];
            } else {
                // Jeśli tylko drugi wierzchołek należał do jednej z grup (tutaj drugiej grupy), to dodaję do tej grupy również pierwszy wierzchołek.
                [destVertexGroup addObject:@(sourceVertex)];
            }
        } else {
            
            // Skoro jestem w tym warunku, tzn. że pierwszy wierzchołek znajduje się w jednej z grup.
            if (!destVertexGroup) {
                
                // Jeśli drugi wierzchołek nie znajduje się w żadnej z grup, to dodaję krawędź do drzewa, a wierzchołek do zbioru, w którym znajduje się pierwszy wierzchołek.
                [self.minimumSpanningTree addEdge:edge];
                [sourceVertexGroup addObject:@(destVertex)];
                
            }
            // Jeśli zaś obydwa wierzchołki znajdują się w jakichś grupach, to sprawdzam, czy nie jest to ta sama grupa.
            // Jeśli jest to ta sama grupa, to nie mogę dodać krawędzi do drzewa, ponieważ utworzyłby się cykl.
            else if (sourceVertexGroup != destVertexGroup) {
                
                // Skoro są to różne grupy, to mogę dodać krawędź do drzewa, ponieważ cyklu nie będzie.
                // Następnie dodaję wszystkie wierzchołki z grupy drugiej do grupy pierwszej i usuwam grupę drugiego wierzchołka ze zbioru wszysztkich grup.
                [self.minimumSpanningTree addEdge:edge];
                
                for (NSNumber *n in destVertexGroup) {
                    [sourceVertexGroup addObject:n];
                }
                [self.vertexGroups removeObject:destVertexGroup];
                
                // Jeśli mam już wszystkie wierzchołki w minimalnym drzewie rozpinającym to mogę zakończyć przechodzenie po krawędziach
                if ([self.graph.vertexes isEqualToSet:self.minimumSpanningTree.vertexes]) {
                    break;
                }
            }
        }
    }
    
    [self printResult];
}

@end
