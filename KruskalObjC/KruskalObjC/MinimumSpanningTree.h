//
//  MinimumSpanningTree.h
//  KruskalObjC
//
//  Created by Mateusz Soliwoda on 23.06.2015.
//  Copyright (c) 2015 Mateusz Soliwoda. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Edge;

@interface MinimumSpanningTree : NSObject

@property (nonatomic, strong) NSMutableSet *edges;
@property (nonatomic, strong) NSMutableSet *vertexes;

- (void)addEdge:(Edge *)edge;

@end
