//
//  Edge.h
//  KruskalObjC
//
//  Created by Mateusz Soliwoda on 15.05.2015.
//  Copyright (c) 2015 Mateusz Soliwoda. All rights reserved.
//
//  s13215@pja.edu.pl

#import <Foundation/Foundation.h>

@interface Edge : NSObject

@property (nonatomic) int weight;
@property (nonatomic) int sourceVert;
@property (nonatomic) int destVert;

- (instancetype)initWithSourceVert:(int)sourceVert destVert:(int)destVert weight:(int)weight;

- (NSString *)toString;

@end
