//
//  ConnectedGraph.m
//  KruskalObjC
//
//  Created by Mateusz Soliwoda on 15.05.2015.
//  Copyright (c) 2015 Mateusz Soliwoda. All rights reserved.
//
//  s13215@pja.edu.pl

#import "ConnectedGraph.h"

@implementation ConnectedGraph

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.edges = nil;
        self.vertexes = nil;
    }
    return self;
}

/**
 *  Sortowanie krawędzi według wag
 */
- (void)sortEdgesIncreasing
{
    NSSortDescriptor *sortDescr = [[NSSortDescriptor alloc] initWithKey:@"weight" ascending:YES];
    [self.edges sortUsingDescriptors:@[sortDescr]];
}

- (NSString *)edgesString
{
    NSMutableString *edgesStr = [[NSMutableString alloc] init];
    
    for (Edge *e in self.edges) {
        [edgesStr appendString:[NSString stringWithFormat:@"(%ld, %ld) : %ld\n\n", (long)e.sourceVert, (long)e.destVert, (long)e.weight]];
    }
    
    return [edgesStr copy];
}

@end
