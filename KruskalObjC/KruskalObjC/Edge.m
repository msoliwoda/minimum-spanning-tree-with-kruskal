//
//  Edge.m
//  KruskalObjC
//
//  Created by Mateusz Soliwoda on 15.05.2015.
//  Copyright (c) 2015 Mateusz Soliwoda. All rights reserved.
//
//  s13215@pja.edu.pl

#import "Edge.h"

@implementation Edge

- (instancetype)initWithSourceVert:(int)sourceVert destVert:(int)destVert weight:(int)weight
{
    self = [super init];
    if (self) {
        self.sourceVert = sourceVert;
        self.destVert = destVert;
        self.weight = weight;
    }
    return self;
}

- (NSString *)toString
{
    return [NSString stringWithFormat:@"(%ld, %ld) : %ld\n\n", (long)self.sourceVert, (long)self.destVert, (long)self.weight];
}

@end
