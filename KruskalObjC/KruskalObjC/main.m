//
//  main.m
//  KruskalObjC
//
//  Created by Mateusz Soliwoda on 15.05.2015.
//  Copyright (c) 2015 Mateusz Soliwoda. All rights reserved.
//
//  s13215@pja.edu.pl

#import <Foundation/Foundation.h>
#import "KruskalAlgorithm.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        KruskalAlgorithm *kruskal = [[KruskalAlgorithm alloc] init];
        
        [kruskal start];
    }
    
    
    return 0;
}
