//
//  MinimumSpanningTree.m
//  KruskalObjC
//
//  Created by Mateusz Soliwoda on 23.06.2015.
//  Copyright (c) 2015 Mateusz Soliwoda. All rights reserved.
//

#import "MinimumSpanningTree.h"
#import "Edge.h"

@implementation MinimumSpanningTree


- (instancetype)init
{
    self = [super init];
    if (self) {
        _edges = [[NSMutableSet alloc] init];
        _vertexes = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)addEdge:(Edge *)edge
{
    [self.edges addObject:edge];
    [self.vertexes addObject:@(edge.sourceVert)];
    [self.vertexes addObject:@(edge.destVert)];
}

@end
